
class Interpreter:
    _reserved = [
        "i",
        "o"
    ]

    _operators = [
        "+",
        "-",
        "x",
        "/",
        "=",
        "?"
    ]

    def __init__(self, order_list):
        self.raw_order_list = order_list
        self.order_list = self.__interpret_blocks()
        self.order_list = self.__seprate_orders()

    def __seprate_orders(self):
        result = []
        previuos = None
        for c in self.order_list:
            if self.__need_to_add_new_row(order=c, previous=previuos):
                result.append(c)
            else:
                result[-1] += c
            previuos = c
        return result

    def __need_to_add_new_row(self, order, previous):
        if previous in self._operators:
            return False
        return (not (order in self._operators or order.isnumeric())
                or order in self._reserved)

    def __interpret_blocks(self):
        if not self.__contain_block(self.raw_order_list):
            return self.raw_order_list

        result = self.raw_order_list
        while self.__contain_block(result):
            result = self.__remove_the_most_in_block(result)
        return result

    def __contain_block(self, order_list):
        if order_list.find("{") != -1:
            return True
        return False

    def __remove_the_most_in_block(self, string):
        start_index, end_index = self.__find_the_most_in_block_index(string)
        before = string[:start_index]
        block = self.__interpret_block(string[start_index: end_index + 1])
        after = string[end_index + 1:]
        return before + block + after

    def __find_the_most_in_block_index(self, string):
        start_index = -1
        end_index = -1
        for i in range(len(string)):
            search_result = string[i:].find("{")
            if search_result != -1:
                start_index = search_result + i
                end_index = -1
            search_result = string[i:].find("}")
            if(search_result + i > start_index and end_index == -1):
                end_index = search_result + i
        return start_index, end_index

    def __interpret_block(self, raw_block):
        loop_number = raw_block[1]
        return raw_block[2:-1] * int(loop_number)

    def get_interpreted_order_list(self):
        return self.order_list
