from utilities.math_tools import Calculator
import settings


class Employee:
    _arithmetic_operators = [
        "+",
        "-",
        "x",
        "/"
    ]

    _conditional_operators = {
        "==",
    }

    def __init__(self, get_input, input_count):
        self.__input_count = input_count
        self.__get_input = get_input
        self._hand = None
        self._error = False
        self._variables = {}

    def __inbox(self):
        if self.__input_count() == 0:
            return settings.ERROR

        self._hand = self.__get_input()
        return settings.SUCCEED

    def __outbox(self):
        if self._hand == None:
            return settings.ERROR, None

        print(self._hand, end=".")
        result = str(self._hand) + "."
        self._hand = None
        return settings.SUCCEED, result

    def execute_order(self, order):
        if not self.__is_execution_continueable():
            return settings.PROCESS_FINSHED, None

        message, result = self.__manage_execution(order)
        if message == settings.ERROR:
            self._error = True
            print(settings.ERROR)
            return settings.ERROR, None

        return message, result

    def __is_execution_continueable(self):
        return (not self._error)

    def __manage_execution(self, order):
        if order == "i":
            return self.__inbox(), None
        elif order == "o":
            message, result = self.__outbox()
            return message, result
        elif "?" in order:
            return self.__handle_condition(order), None
        elif self.__need_stack(order):
            return self.__handle_stack(order), None
        elif self.__contain_arithmetic_operators(order):
            return self.__do_math(order), None
        elif "=" in order:
            return self.__init_variable(order), None
        elif order[0] in self._variables:
            return self.__read_variable(order), None
        else:
            return settings.ERROR, None

    def __handle_condition(self, order):
        splited = order.split('?')
        condition_result = False
        if self.__calculate_condition(splited[0]):
            return self.__manage_execution(splited[1])
        return settings.SUCCEED

    def __calculate_condition(self, condition):
        splited = condition.split('==')

        l = self.__get_statement_value(splited[0])
        r = self.__get_statement_value(splited[1])
        return l == r

    def __get_statement_value(self, statement):
        if statement.isnumeric():
            return int(statement)
        self._hand = self._variables[statement]
        return self._hand

    def __do_math(self, order):
        extracted = self.__extract_expression(order)
        c = Calculator()
        p = ''.join(str(e) for e in extracted)
        self._hand = c.calculate(p)
        # self.__calculate_extracted(extracted)
        return settings.SUCCEED

    def __calculate_extracted(self, expression):
        counter = 0
        result = 0
        expression.insert(0, "+")
        for e in expression:
            counter += 1
            if e in self._arithmetic_operators:
                result = self._arithmetic_operators[e](
                    result, expression[counter])
                self._hand = str(result)

    def __extract_expression(self, order):
        result = []
        number = 0
        p = None
        for o in order:
            if o in self._arithmetic_operators:
                p = self.__add_prevoius(p, number)
                result.append(p)
                result.append(o)
                number = 0
            elif o.isdigit():
                number *= 10
                number += int(o)
            p = o
        o = self.__add_prevoius(o, number)
        result.append(o)
        return result

    def __add_prevoius(self, previous, number):
        if previous == None:
            return number
        return (int(self._variables[previous])
                if previous in self._variables else number)

    def __handle_stack(self, order):
        seprated = order.split("=")
        m, r = self.__manage_execution(seprated[1])
        if r == None:
            new_order = seprated[0] + "=" + str(self._hand)
            return self.__manage_execution(new_order)
        return m

    def __contain_arithmetic_operators(self, order):
        for a in self._arithmetic_operators:
            splited = order.split(a)
            if len(splited) > 1:
                if len(splited[0]) > 0 and len(splited[1]) > 0:
                    return True
        return False

    def __need_stack(self, order):
        if not "=" in order:
            return False
        return self.__contain_arithmetic_operators(order)

    def __init_variable(self, order):
        seprated = order.split("=")
        if(seprated[1].isnumeric()):
            value = int(seprated[1])
        elif (seprated[1] == "i"):
            self.__inbox()
            value = self._hand
        else:
            value = self._variables[seprated[1]]
        self._variables[seprated[0]] = value
        self._hand = value
        return settings.SUCCEED

    def __read_variable(self, order):
        self._hand = self._variables[order]
        return settings.SUCCEED

    def get_hand_content(self):
        return self._hand

    def get_variables(self):
        return self._variables
