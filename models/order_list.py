class OrderList:
    def __init__(self):
        self._order_list = ""
        pass

    def get_user_orders(self):
        done = False
        while not done:
            inp = input()
            line = ""
            for i in inp:
                if i != " ":
                    line += i
            if line != "End":
                self._order_list += line
            else:
                done = True
        return self._order_list

    def __len__(self):
        return len(self._order_list)

    def __getitem__(self, index):
        return self._order_list[index]

    def __str__(self) -> str:
        return self._order_list
