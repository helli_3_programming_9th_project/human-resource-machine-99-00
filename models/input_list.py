import random


class Input:
    def __init__(self, pattern):
        self._correct_answer = None
        self._input = ""
        if "o" in pattern:
            pattern = pattern.split("o")
            c = int(pattern[0])
            f = int(pattern[1])
            self._input = self.__generate_in_order_input(c, f)

        if "r" in pattern:
            pattern = pattern.split("r")
            c = int(pattern[0])
            self._input = self.__generate_random_input(c)

    def __generate_in_order_input(self, count, start):
        return [i for i in range(start, start + count)]

    def __generate_random_input(self, count):
        return [random.randint(0, 9) for i in range(count)]

    def generate_new_inbox(self):
        result = ""
        for i in range(7):
            result += (str(random.randint(0, 9)) + ".")
        result = result[0: -1]
        self._correct_answer = result[::-1] + "."
        return result.split(".")

    def is_user_result_satisfactory(self, user_result):
        return self._correct_answer == user_result

    def get_input(self):
        return self._input.pop()

    def is_empty(self):
        return len(self._input) <= 0

    def count(self):
        return len(self._input)
