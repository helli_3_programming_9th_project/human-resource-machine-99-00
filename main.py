from models.employee import Employee
from models.interpreter import Interpreter
from models.input_list import Input
from models.order_list import OrderList


order_list = OrderList()
order_list.get_user_orders()
inp = Input("7o0")
employee = Employee(inp.get_input, inp.count)
interpreter = Interpreter(str(order_list))
order_list = interpreter.get_interpreted_order_list()
print("order list:", order_list)
result = ""
while not inp.is_empty() and len(order_list) > 0:
    message, temp_result = employee.execute_order(order_list[0])
    del(order_list[0])
    if temp_result != None:
        result += temp_result
    print("variables:", employee.get_variables(),
          "in hand: ", employee.get_hand_content())

print(result)
