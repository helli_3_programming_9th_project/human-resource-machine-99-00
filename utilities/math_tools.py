class ArithmeticOperation:
    def __init__(
            self, order, under_operation_related_index,
            constraint_data=None, constraint_meter=None):

        self._order = order
        self._constraint_data = constraint_data
        self._under_operation_related_index = under_operation_related_index
        self.__constraint_meter = constraint_meter
        pass

    def __validate(self, parameter):
        if self.__constraint_meter is None:
            return True
        if self.__constraint_meter(parameter):
            return True
        return False

    def operate(self, *args):
        pass

    def get_order(self):
        return self._order

    def get_under_operation_related_indexs(self):
        return self._under_operation_related_index


class TwoVariableAtLessOperation(ArithmeticOperation):
    def __init__(self, order,):
        constraint_data = {
            "min parameter": 2,
        }
        super().__init__(
            order,
            under_operation_related_index=[-1, 1],
            constraint_data=constraint_data,
            constraint_meter=self.__constraint_meter)

    def __constraint_meter(self, parameter):
        if len(parameter) > self._constraint_data["min parameter"]:
            return True
        return False


class OnlyOneVariableOperation(ArithmeticOperation):
    def __init__(self, order):
        constraint_data = {
            "parameter count": 1,
        }
        super().__init__(
            order,
            under_operation_related_index=[-1],
            constraint_data=constraint_data,
            constraint_meter=self.__constraint_meter)

    def __constraint_meter(self, parameter):
        if len(parameter) == self._constraint_data["parameter count"]:
            return True
        return False


class SumClass(TwoVariableAtLessOperation):
    def __init__(self):
        super().__init__(order=2)


class MultiplicationClass(TwoVariableAtLessOperation):
    def __init__(self):
        super().__init__(order=1)


class Sum(SumClass):
    def operate(self, *args):
        result = 0
        for arg in args:
            result += arg
        return result


class Min(SumClass):
    def operate(self, *args):
        result = args[0]
        for arg in args[1:]:
            result -= arg
        return result


class Star(SumClass):
    def operate(self, *args):
        result = args[0]
        for arg in args[1:]:
            result += arg
            result *= 2
        return result


class Multiplication(MultiplicationClass):
    def operate(self, *args):
        result = 1
        for arg in args:
            result *= arg
        return result


class Div(MultiplicationClass):
    def operate(self, *args):
        result = args[0]
        for arg in args[1:]:
            result /= arg
        return result


class Factoriel(OnlyOneVariableOperation):
    def __init__(self):
        super().__init__(order=0)

    def operate(self, n):
        result = 1
        for i in range(n):
            result *= (i + 1)
        return result


class Calculator:
    _operators = {
        "+": Sum(),
        "-": Min(),
        "*": Star(),
        "!": Factoriel(),
        "x": Multiplication(),
        "/": Div()
    }

    def calculate(self, phrase):
        self._phrase = phrase
        self.__extract()
        while self.__any_operation_left():
            self.__solve_highest_precedence()
        return self._extracted_phrase[0]

    def __any_operation_left(self):
        for o in self._operators:
            if o in self._extracted_phrase:
                return True
        return False

    def __solve_highest_precedence(self):
        index = self.__find_highest_precedence()
        self.__replace_operation_with_value(index)

    def __replace_operation_with_value(self, index):
        operator_charecter = self._extracted_phrase[index]
        operator = self._operators[operator_charecter]
        under_operation_numbers = []
        indexes = operator.get_under_operation_related_indexs()
        indexes.sort()
        for i in indexes:
            under_operation_numbers.append(self._extracted_phrase[index + i])
        self._extracted_phrase[index] = operator.operate(
            *under_operation_numbers)

        indexes.reverse()
        for i in indexes:
            del(self._extracted_phrase[index + i])

    def __find_highest_precedence(self):
        c = 0
        highest_o = None
        index = None
        for e in self._extracted_phrase:
            if e in self._operators:
                o = self._operators[e]
                if (index is None or o.get_order() < highest_o.get_order()):
                    highest_o = o
                    index = c
            c += 1
        return index

    def __extract(self):
        self._extracted_phrase = []
        number = 0
        for e in self._phrase:
            if e.isnumeric():
                if number is None:
                    number = 0
                number *= 10
                number += int(e)
            else:
                if not number is None:
                    self._extracted_phrase.append(number)
                self._extracted_phrase.append(e)
                number = None
        if not number is None:
            self._extracted_phrase.append(number)
        return self._extracted_phrase
